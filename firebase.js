import { initializeApp, getApps, getApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyC6na444NsSMoeYMcxPLxEwUy-VygZemNA",
  authDomain: "ubereats-db.firebaseapp.com",
  projectId: "ubereats-db",
  storageBucket: "ubereats-db.appspot.com",
  messagingSenderId: "269254934801",
  appId: "1:269254934801:web:584d090f76bd25ab804cb0",
};

const app = !getApps().length ? initializeApp(firebaseConfig) : getApp();
const db = getFirestore();

export { app, db };
