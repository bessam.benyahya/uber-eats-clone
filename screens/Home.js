import { useEffect, useState } from "react";
import { View, SafeAreaView, ScrollView } from "react-native";
import Categories from "../components/Categories";
import HeaderTabs from "../components/header/HeaderTabs";
import RestaurantItems from "../components/restaurant-home/RestaurantItems";
import SearchBar from "../components/SearchBar";
import { YELP_API_KEY } from "@env";
import { Divider } from "react-native-elements";
import BottomTabs from "../components/BottomTabs";

const DELIVERY_MODE = "Delivery";
const PICKUP_MODE = "Pickup";

const Home = ({ navigation }) => {
  const [restaurantsData, setRestaurantsData] = useState([]);
  const [city, setCity] = useState("New York");
  const [activeTab, setActiveTab] = useState(DELIVERY_MODE);

  const getRestaurantsFromYelp = () => {
    const yelpUrl = `https://api.yelp.com/v3/businesses/search?term=food&location=${city}`;

    const apiOptions = {
      headers: {
        Authorization: `Bearer ${YELP_API_KEY}`,
      },
    };

    return fetch(yelpUrl, apiOptions)
      .then((res) => res.json())
      .then((res) =>
        setRestaurantsData(
          res.businesses.filter((business) =>
            business.transactions.includes(activeTab.toLowerCase())
          )
        )
      );
  };

  useEffect(() => {
    getRestaurantsFromYelp();
  }, [city, activeTab]);

  return (
    <SafeAreaView style={{ backgroundColor: "#eee", flex: 1 }}>
      <View style={{ backgroundColor: "#fff", padding: 15 }}>
        <HeaderTabs activeTab={activeTab} setActiveTab={setActiveTab} />
        <SearchBar cityHandler={setCity} />
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <Categories />
        <RestaurantItems
          restaurantsData={restaurantsData}
          navigation={navigation}
        />
      </ScrollView>
      <Divider width={1} />
      <BottomTabs />
    </SafeAreaView>
  );
};

export default Home;
