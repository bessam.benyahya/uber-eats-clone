import { View } from "react-native";
import { Divider } from "react-native-elements";
import About from "../components/restaurant-details/About";
import MenuItems from "../components/restaurant-details/MenuItems";
import ViewCart from "../components/restaurant-details/ViewCart";

import { foods } from "../DUMMY_DATA";

export default function RestaurantDetails({ route, navigation }) {
  const { name } = route.params;
  return (
    <View>
      <About route={route} />
      <Divider width={1.8} style={{ marginVertical: 20 }} />
      <MenuItems restaurantName={name} foods={foods} />
      <ViewCart navigation={navigation} restaurantName={name} />
    </View>
  );
}
