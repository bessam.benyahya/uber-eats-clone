import { foods } from "../DUMMY_DATA";

import { Text, SafeAreaView, View } from "react-native";
import LottieView from "lottie-react-native";
import MenuItems from "../components/restaurant-details/MenuItems";
import { db } from "../firebase";
import {
  collection,
  onSnapshot,
  orderBy,
  query,
  limit,
} from "firebase/firestore";
import { useEffect, useState } from "react";
import { ScrollView } from "react-native-gesture-handler";

export default function OrderCompleted({ restaurantName }) {
  const [lastOrder, setLastOrder] = useState(null);

  const total = lastOrder?.items
    .map((item) => Number(item.price.replace("$", "")))
    .reduce((acc, curr) => acc + curr, 0);

  const totalUSD = (total || 0).toLocaleString("en", {
    style: "currency",
    currency: "USD",
  });

  useEffect(
    () =>
      onSnapshot(
        query(collection(db, "orders"), orderBy("timestamp", "desc"), limit(1)),
        (snapshot) => {
          snapshot.docs.map((doc) => {
            setLastOrder(doc.data());
          });
        }
      ),
    [db]
  );

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: "#fff" }}>
      <View
        style={{
          margin: 15,
          alignItems: "center",
          height: "100%",
        }}
      >
        <LottieView
          style={{
            height: 170,
            alignSelf: "center",
            marginBottom: 30,
          }}
          source={require("../assets/animations/done.json")}
          speed={1}
        />

        <Text style={{ fontSize: 20, fontWeight: "bold", marginBottom: 20 }}>
          Your order at {restaurantName} has been placed for {totalUSD}
        </Text>
        {lastOrder && lastOrder.items && (
          <ScrollView>
            <MenuItems
              foods={lastOrder.items}
              hideCheckbox={true}
              marginLeft={10}
            />
          </ScrollView>
        )}
        <LottieView
          style={{
            height: 150,
            alignSelf: "center",
            marginBottom: 30,
          }}
          source={require("../assets/animations/cooking.json")}
          speed={0.7}
          autoPlay
        />
      </View>
    </SafeAreaView>
  );
}
