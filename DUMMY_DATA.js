export const categories = [
  {
    image: require("./assets/images/shopping-bag.png"),
    text: "Pick-up",
  },
  {
    image: require("./assets/images/bread.png"),
    text: "Bakery",
  },
  {
    image: require("./assets/images/fast-food.png"),
    text: "Fast Foods",
  },
  {
    image: require("./assets/images/deals.png"),
    text: "Deals",
  },
  {
    image: require("./assets/images/coffee.png"),
    text: "Coffee & Tea",
  },
];

export const localRestaurants = [
  {
    name: "Chamas Tacos - Chatelet Les Halles",
    image:
      "https://res.feednews.com/assets/v2/94e49246c6c2d42943e805ff74ae73e5?width=1280&height=720&quality=hq&category=us_Economy&Finance",
    categories: [{ title: "Tacos" }, { title: "Comfort Food" }],
    price: "$",
    reviews: 1244,
    rating: 4.7,
  },
  {
    name: "Chamas Tacos - Bastille",
    image:
      "https://uploads.lebonbon.fr/source/2021/june/2020415/exterieur_2_1200.jpg",
    categories: [{ title: "Tacos" }, { title: "Comfort Food" }],
    price: "$",
    reviews: 927,
    rating: 4.6,
  },
  {
    name: "Chamas Tacos - Créteil",
    image:
      "https://www.franchise-magazine.com/wp-content/uploads/Franchise-Restaurant-Chamas-Tacos-comptoir.jpg",
    categories: [{ title: "Tacos" }, { title: "Comfort Food" }],
    price: "$",
    reviews: 2128,
    rating: 4.8,
  },
];

export const foods = [
  {
    title: "Sur-Mesure Gratiné",
    description:
      "Tailles et viandes au choix, sauces au choix & sauce fromagère secrète",
    image:
      "https://d1ralsognjng37.cloudfront.net/b50bd469-9473-41df-adea-3c3900d8f83c.jpeg",
    price: "$8.90",
  },
  {
    title: "Tacos classique + Boisson",
    description:
      "Taille au choix, viandes au choix & sauce et une boisson (33cl)",
    image:
      "https://d1ralsognjng37.cloudfront.net/3f06bc0a-ecd2-4eb0-a7e6-a8f57a3b27eb.jpeg",
    price: "$9.90",
  },
  {
    title: "Tacos gratiné + Boisson",
    description:
      "Taille au choix, viandes au choix & sauce et une boisson (33cl)",
    image:
      "https://d1ralsognjng37.cloudfront.net/266bbab9-f0a7-4ac0-8718-7924a4e5decd.jpeg",
    price: "$10.90",
  },
  {
    title: "Sur-Mesure Gratiné BIS",
    description:
      "Tailles et viandes au choix, sauces au choix & sauce fromagère secrète",
    image:
      "https://d1ralsognjng37.cloudfront.net/b50bd469-9473-41df-adea-3c3900d8f83c.jpeg",
    price: "$8.90",
  },
  {
    title: "Tacos classique + Boisson BIS",
    description:
      "Taille au choix, viandes au choix & sauce et une boisson (33cl)",
    image:
      "https://d1ralsognjng37.cloudfront.net/3f06bc0a-ecd2-4eb0-a7e6-a8f57a3b27eb.jpeg",
    price: "$9.90",
  },
  {
    title: "Tacos gratiné + Boisson BIS",
    description:
      "Taille au choix, viandes au choix & sauce et une boisson (33cl)",
    image:
      "https://d1ralsognjng37.cloudfront.net/266bbab9-f0a7-4ac0-8718-7924a4e5decd.jpeg",
    price: "$10.90",
  },
];
