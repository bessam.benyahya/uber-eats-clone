import { View, Text } from "react-native";

export default function RestaurantInfo({ name, rating, reviews }) {
  return (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        marginTop: 10,
      }}
    >
      <View>
        <Text style={{ fontSize: 15, fontWeight: "bold" }}>{name}</Text>
        <Text style={{ fontSize: 13, color: "gray" }}>Entre 15 et 25 min</Text>
      </View>
      <View
        style={{
          backgroundColor: "#eee",
          alignItems: "center",
          justifyContent: "center",
          borderRadius: 15,
          height: 30,
          paddingRight: 10,
          paddingLeft: 10,
        }}
      >
        <Text>
          {rating} ({reviews})
        </Text>
      </View>
    </View>
  );
}
