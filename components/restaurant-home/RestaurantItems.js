import { View, TouchableOpacity } from "react-native";
import RestaurantImage from "./RestaurantImage";
import RestaurantInfo from "./RestaurantInfo";

export default function RestaurantItems({ navigation, restaurantsData }) {
  return (
    <>
      {restaurantsData.map(
        ({ name, rating, image_url, review_count, categories }, index) => (
          <TouchableOpacity
            key={index}
            style={{ marginBottom: 30 }}
            activeOpacity={1}
            onPress={() =>
              navigation.navigate("RestaurantDetails", {
                name,
                rating,
                image: image_url,
                reviews: review_count,
                categories,
              })
            }
          >
            <View
              key={index}
              style={{ marginTop: 10, padding: 15, backgroundColor: "#fff" }}
            >
              <RestaurantImage image={image_url} />
              <RestaurantInfo
                name={name}
                rating={rating}
                reviews={review_count}
              />
            </View>
          </TouchableOpacity>
        )
      )}
    </>
  );
}
