import { Image } from "react-native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

export default function RestaurantImage({ image }) {
  return (
    <>
      <Image
        source={{
          uri: image ? `${image}` : null,
        }}
        style={{ width: "100%", height: 180 }}
      />
    </>
  );
}
