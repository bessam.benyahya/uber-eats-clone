import { Text } from "react-native";

export default function RestaurantTitle({ title }) {
  return (
    <Text
      style={{
        fontSize: 29,
        fontWeight: "600",
        marginTop: 10,
        marginHorizontal: 15,
      }}
    >
      {title}
    </Text>
  );
}
