import { View } from "react-native";
import RestaurantImage from "./RestaurantImage";
import RestaurantTitle from "./RestaurantTitle";
import RestaurantDescription from "./RestaurantDescription";

export default function About({ route }) {
  const { name, image, categories, rating, reviews, price } = route.params;

  const formattedCategories = categories.map((cat) => cat.title).join(" . ");
  const description = `${formattedCategories}${
    price ? " . " + price : ""
  } . 💳  . ${rating}⭐ (${reviews}+)`;

  return (
    <View>
      <RestaurantImage image={image} />
      <RestaurantTitle title={name} />
      <RestaurantDescription description={description} />
    </View>
  );
}
