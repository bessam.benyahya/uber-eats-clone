import { useState } from "react";
import { View } from "react-native";
import HeaderButton from "./HeaderButton";

const DELIVERY_MODE = "Delivery";
const PICKUP_MODE = "Pickup";

const HeaderTabs = ({ activeTab, setActiveTab }) => {
  return (
    <View
      style={{
        flexDirection: "row",
        alignSelf: "center",
      }}
    >
      <HeaderButton
        text={DELIVERY_MODE}
        activeTab={activeTab}
        setActiveTab={setActiveTab}
      />
      <HeaderButton
        text={PICKUP_MODE}
        activeTab={activeTab}
        setActiveTab={setActiveTab}
      />
    </View>
  );
};

export default HeaderTabs;
